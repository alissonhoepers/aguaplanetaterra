package br.sc.aguaplanetaterra.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value="cadastroPlayerController")
@SessionScoped
public class CadastroPlayerController implements Serializable{
	
	private static final long serialVersionUID  = 1l;
	
	private String nomeJogador;
	private String sobrenomeJogador;
	private Integer idade;

	public String getSobrenomeJogador() {
		return sobrenomeJogador;
	}

	public void setSobrenomeJogador(String sobrenomeJogador) {
		this.sobrenomeJogador = sobrenomeJogador;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}
	
	

}
