package br.sc.aguaplanetaterra.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value="paginaInicialController")
@SessionScoped
public class PaginaInicialController implements Serializable{

	private static final long serialVersionUID = 1L;

	private String dizeres = "Reduzo o consumo.";

	public String getDizeres() {
		return dizeres;
	}

	public void setDizeres(String dizeres) {
		this.dizeres = dizeres;
	}
}
